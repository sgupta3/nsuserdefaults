//
//  AppDelegate.h
//  NSUserDefaults
//
//  Created by Sahil Gupta on 2015-07-31.
//  Copyright (c) 2015 Sahil Gupta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

