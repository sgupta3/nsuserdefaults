//
//  Car.h
//  NSUserDefaults
//
//  Created by Sahil Gupta on 2015-07-31.
//  Copyright (c) 2015 Sahil Gupta. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Car : NSObject
@property (nonatomic) NSString *make;
@property (nonatomic) NSString *color;

- (id) initWithMake:(NSString *)make color:(NSString *)color;
- (void) encodeWithCoder : (NSCoder *)encode ;
- (id) initWithCoder : (NSCoder *)decode;
@end
