//
//  Car.m
//  NSUserDefaults
//
//  Created by Sahil Gupta on 2015-07-31.
//  Copyright (c) 2015 Sahil Gupta. All rights reserved.
//

#import "Car.h"

@implementation Car

- (id) initWithMake:(NSString *)make color:(NSString *)color {
    self = [super init];
    if (self) {
        _make = make;
        _color = color;
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder;
{
    [coder encodeObject:_make forKey:@"make"];
    [coder encodeObject:_color forKey:@"color"];
}

- (id)initWithCoder:(NSCoder *)coder;
{
    self = [super init];
    if (self != nil)
    {
        _make = [coder decodeObjectForKey:@"make"];
        _color = [coder decodeObjectForKey:@"color"];
    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"Color - %@ | Make - %@",_color,_make];
}




@end
