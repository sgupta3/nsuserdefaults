//
//  ViewController.m
//  NSUserDefaults
//
//  Created by Sahil Gupta on 2015-07-31.
//  Copyright (c) 2015 Sahil Gupta. All rights reserved.
//

#import "ViewController.h"
#import "Car.h"

@interface ViewController ()
@property(nonatomic) NSMutableArray *cars;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _cars = [[NSMutableArray alloc] init];
  
    [_cars addObject:[[Car alloc] initWithMake:@"Honda" color:@"Black"]];
    [_cars addObject:[[Car alloc] initWithMake:@"Mazda" color:@"Gray"]];
    [_cars addObject:[[Car alloc] initWithMake:@"Infiniti" color:@"White"]];
    
    [self writeArrayWithCustomObjToUserDefaults:@"cars" withArray:self.cars];
    
    NSArray *data = [self readArrayWithCustomObjFromUserDefaults:@"cars"];
    
    [data enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Car *currentCar = (Car *)obj;
        NSLog(@"%@",currentCar);
    }];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)writeArrayWithCustomObjToUserDefaults:(NSString *)keyName withArray:(NSMutableArray *)myArray
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:myArray];
    
    [defaults setObject:nil forKey:keyName];
    [defaults synchronize];
    
    
    [defaults setObject:data forKey:keyName];
    [defaults synchronize];
}

-(NSArray *)readArrayWithCustomObjFromUserDefaults:(NSString*)keyName
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:keyName];
    NSArray *myArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    [defaults synchronize];
    return myArray;
}




@end
