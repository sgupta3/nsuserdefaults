//
//  main.m
//  NSUserDefaults
//
//  Created by Sahil Gupta on 2015-07-31.
//  Copyright (c) 2015 Sahil Gupta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
